FROM ubuntu:focal-20201008
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y libicu66 libssl-dev wget

# install dotnet 6
RUN ( mkdir /opt/dotnet6 && cd /opt/dotnet6 && \
  wget https://download.visualstudio.microsoft.com/download/pr/20283373-1d83-4879-8278-0afb7fd4035e/56f204f174743b29a656499ad0fc93c3/dotnet-sdk-6.0.100-rc.2.21505.57-linux-x64.tar.gz && tar -xvf dotnet-sdk-6.0.100-rc.2.21505.57-linux-x64.tar.gz && \
  rm dotnet-sdk-6.0.100-rc.2.21505.57-linux-x64.tar.gz)

